#include <assert.h>
#include <sequence.h>
#include <dict.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <bitStream.h>
#include <lzw.h>
#include <string.h>

typedef struct _context {
    FILE* file;
} Context;
int mywrite(unsigned char, void*);
int myread(void*);
char* concat(char*, char*);

int main(int argc, char * argv[]) {

    Context* writecontext = (Context*) malloc( sizeof(Context) );
    Context* readcontext = (Context*) malloc( sizeof(Context) );

    writecontext->file = fopen( argv[2], "w" );
    readcontext->file = fopen( argv[1], "r" );

    lzwDecode( atoi(argv[3]), atoi(argv[4]), myread, readcontext, mywrite, writecontext );

    fclose(writecontext->file);
    fclose(readcontext->file);

    free(readcontext);
    free(writecontext);
}

int mywrite( unsigned char c, void* context) {
    FILE* fileptr = ((Context*)context)->file;
    return fputc(c, fileptr);
}

int myread( void* context ) {
    FILE* fileptr = ((Context*)context)->file;
    return fgetc(fileptr);
}
