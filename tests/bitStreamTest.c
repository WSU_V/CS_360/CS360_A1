#include <assert.h>
#include <sequence.h>
#include <dict.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <bitStream.h>
typedef struct _context {
    FILE* file;
} Context;

int mywrite(unsigned char, void*);
int myread(void* context);

int main() {

    Context* contextwrite = (Context*) malloc( sizeof(Context) );
    Context* contextread = (Context*) malloc( sizeof(Context) );



    unsigned int incode = 100;
    unsigned int outcode;

		for ( int i = 8; i <= 24; i++ ) {
			outcode = 0;
			contextwrite->file = fopen("deleteme.txt", "w");
			BitStream* out = openOutputBitStream( mywrite, contextwrite );
			contextread->file = fopen("deleteme.txt", "r");
			BitStream* in = openInputBitStream( myread, contextread );
			outputBits(out, i, incode);
			closeAndDeleteBitStream( out );
			fclose(contextwrite->file);
			readInBits(in, i, &outcode);
			fclose(contextread->file);
			assert(outcode == incode);
			closeAndDeleteBitStream( in );
		}
		contextwrite->file = fopen("deleteme.txt", "w");
		BitStream* out = openOutputBitStream( mywrite, contextwrite );
		for ( int i = 8, j = 32; i <=24; i++, j++) {
			outputBits(out, i, j);
		}
		closeAndDeleteBitStream( out );
		fclose(contextwrite->file);
		contextread->file = fopen("deleteme.txt", "r");
		BitStream* in = openInputBitStream( myread, contextread );
		for (int i = 8, j = 32; i<=24; i++, j++ ) {
			readInBits(in, i, &outcode);
			//printf("j = %d | read = %d\n", j, outcode);
			assert( outcode == j );
		}
		closeAndDeleteBitStream( in );
		fclose(contextread->file);
		free( contextwrite );
		free( contextread );

    return 0;
}

int mywrite( unsigned char c, void* context) {
    FILE* fileptr = ((Context*)context)->file;
    return fputc(c, fileptr);
}

int myread( void* context ) {
    FILE* fileptr = ((Context*)context)->file;
    return fgetc(fileptr);
}
