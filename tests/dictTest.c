#include <assert.h>
#include <sequence.h>
#include <dict.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main () {

	Dict* dict = newDict( 100000 );

    insertIntoDict( dict, newSequence( 'a' ), 256 );

    unsigned int code = 0;

    bool i = searchDict( dict, newSequence( 'a' ), &code );

    assert ( i == true );
    assert ( code == 256 );

    return 0;
}
