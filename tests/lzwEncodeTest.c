#include <assert.h>
#include <sequence.h>
#include <dict.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <bitStream.h>
#include <lzw.h>
#include <string.h>

typedef struct _context {
    FILE* file;
} Context;
int mywrite(unsigned char, void*);
int myread(void*);
char* concat(char*, char*);

int main(int argc, char *argv[] ) {
    char compressedname[ 50 ];
    strcpy( compressedname, argv[1] );
    strcat( compressedname, ".lzw" );


    Context* writecontext = (Context*) malloc( sizeof(Context) );
    Context* readcontext = (Context*) malloc( sizeof(Context) );
    writecontext->file = fopen( compressedname, "w" );
    assert(writecontext->file != NULL);
    readcontext->file = fopen( argv[1], "r" );
		assert(readcontext->file != NULL);
    lzwEncode( atoi(argv[2]), atoi(argv[3]) , myread, readcontext, mywrite, writecontext );
    fclose(writecontext->file);
    fclose(readcontext->file);
    free ( writecontext );
    free ( readcontext );
    return 0;
}

int mywrite( unsigned char c, void* context) {
    FILE* fileptr = ((Context*)context)->file;
    return fputc(c, fileptr);
}

int myread( void* context ) {

    FILE* fileptr = ((Context*)context)->file;
    int c = (int)fgetc(fileptr);
    return c;
}
