compileEncode () {
	gcc lzwEncodeTest.c ../lzwLib/lzwEncode.c ../lzwLib/dict.c ../lzwLib/sequence.c ../lzwLib/bitStream.c -I../lzwLib/include -std=c99 -o lzwEncode.test
}

compileDecode () {
	gcc lzwDecodeTest.c ../lzwLib/lzwDecode.c ../lzwLib/dict.c ../lzwLib/sequence.c ../lzwLib/bitStream.c -I../lzwLib/include -std=c99 -o lzwDecode.test
}

runBitStream() {
	gcc bitStreamTest.c ../lzwLib/bitStream.c -I../lzwLib/include -std=c99 -g -o bitStream.test
	./bitStream.test
	rm ./bitStream.test
}
exuastTest () {

	./lzwEncode.test largetext.txt $1 $2
	./lzwForOSX -decode -startingBits $1 -maximumBits $2 -input largetext.txt.lzw -output deleteme
	cmp deleteme largetext.txt || echo "not same encode"
	#rm deleteme

	./lzwDecode.test largetext.txt.lzw deleteme $1 $2
	cmp deleteme largetext.txt || echo "not same decode"
	#rm deleteme largetext.txt.lzw

	./lzwForOSX -encode -startingBits $1 -maximumBits $2 -input largetext.txt -output largetext.txt.lzw
	./lzwDecode.test largetext.txt.lzw deleteme $1 $2
	cmp deleteme largetext.txt || echo "not same decode"
	#rm deleteme largetext.txt.lzw

	./lzwEncode.test largetext.txt $1 $2
	./lzwDecode.test largetext.txt.lzw deleteme $1 $2
	cmp deleteme largetext.txt || echo "not same decode"
	#rm deleteme largetext.txt.lzw
	echo "FINISHED"

}

#test files for encode and decode
testfile="largetext.txt"
testfilecmp=$testfile".lzw"
testfiledec="largetext-1.txt"


if [ $1 == "decode" ]
then
	echo "---- RUNNING DECODE ----"
	compileDecode
	./lzwDecode.test $testfilecmp $testfiledec $2 $3
	rm ./lzwDecode.test
	exit
elif [ $1 == "encode" ]
then
	echo "---- RUNNING ENCODE ----"
	compileEncode
	./lzwEncode.test $testfile $2 $3
	rm ./lzwEncode.test
	exit
elif [ $1 == "bit" ]
then
	runBitStream
	exit
elif [ $1 == "final" ]
then
	rm *.test *.lzw largetext-1.txt
	compileDecode
	compileEncode
	for start in `seq 8 24`
	do
		for max in `seq $start 24`
		do
			echo "testing on $start $max"
			exuastTest $start $max
		done
	done
	rm *.test *.lzw largetext-1.txt
	exit
else
	echo "---- RUNNING ALL TESTS ------"
fi





#./lzwForOSX -decode -input testfile.txt.lzw-output uncompress.txt


echo "---- COMPILING dictTest.c TESTS ----"
gcc dictTest.c ../lzwLib/sequence.c ../lzwLib/dict.c -I../lzwLib/include -std=c99 -o dict.test
echo "---- STARTING dict.test TESTS ----"
./dict.test
echo "---- FINISHED dict.test TESTS ----"
rm ./dict.test


echo "--- COMPLILING bitStreamTest.c TESTS ---"
gcc bitStreamTest.c ../lzwLib/bitStream.c -I../lzwLib/include -std=c99 -o bitStream.test
echo "---- STARTING bitStream.test TESTS ----"
./bitStream.test
echo "---- FINISHED bitStream.test TESTS (delete deleteme.txt)----"
rm ./bitStream.test
rm ./deleteme.txt


echo "---- COMPILING lzwEncodeTest.c TESTS --- "
gcc lzwEncodeTest.c ../lzwLib/lzwEncode.c ../lzwLib/dict.c ../lzwLib/sequence.c ../lzwLib/bitStream.c -I../lzwLib/include -std=c99 -o lzwEncode.test
echo "---- STARTING lzwEncode.test TESTS ----"
rm $testfilecmp
#valgrind --leak-check=full ./lzwEncode.test $testfile
./lzwEncode.test $testfile
echo "---- FINISHED dict.test TESTS ----"
rm lzwEncode.test


echo "---- COMPILING lzwDecodeTest.c TESTS ----"
gcc lzwDecodeTest.c ../lzwLib/lzwDecode.c ../lzwLib/dict.c ../lzwLib/sequence.c ../lzwLib/bitStream.c -I../lzwLib/include -std=c99 -o lzwDecode.test
echo "---- STARTING lzwDecode.test TESTS ----"
rm $testfiledec
#valgrind --leak-check=full ./lzwDecode.test $testfilecmp $testfiledec
./lzwDecode.test $testfilecmp $testfiledec
echo "---- FINISHED lzwDecode.test TESTS ----"
rm ./lzwDecode.test
